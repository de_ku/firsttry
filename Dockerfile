FROM python:3.8

#RUN groupadd --gid 2000 dockeruser
#RUN useradd --uid 2000 --gid dockeruser --shell /bin/bash --create-home dockeruser
#RUN su dockeruser

#COPY requirements/production.txt requirements/production.txt
#RUN sudo pip install --no-cache -r  requirements/production.txt

#COPY dist dist
#RUN pip install dist/*

#COPY run_service.py run_servicer.py 

#EXPOSE 8085

#CMD ["python", "run_servicer.py", "--port", "8085"]
CMD ["/bin/bash"]
